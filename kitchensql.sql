USE [master]
GO
/****** Object:  Database [Kitchen]    Script Date: 2024/2/26 20:43:37 ******/
CREATE DATABASE [Kitchen]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'Kitchen', FILENAME = N'D:\Program Files\Microsoft SQL Server\MSSQL16.MSSQLSERVER\MSSQL\DATA\Kitchen.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'Kitchen_log', FILENAME = N'D:\Program Files\Microsoft SQL Server\MSSQL16.MSSQLSERVER\MSSQL\DATA\Kitchen_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
 WITH CATALOG_COLLATION = DATABASE_DEFAULT, LEDGER = OFF
GO
ALTER DATABASE [Kitchen] SET COMPATIBILITY_LEVEL = 100
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [Kitchen].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [Kitchen] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [Kitchen] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [Kitchen] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [Kitchen] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [Kitchen] SET ARITHABORT OFF 
GO
ALTER DATABASE [Kitchen] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [Kitchen] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [Kitchen] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [Kitchen] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [Kitchen] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [Kitchen] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [Kitchen] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [Kitchen] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [Kitchen] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [Kitchen] SET  DISABLE_BROKER 
GO
ALTER DATABASE [Kitchen] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [Kitchen] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [Kitchen] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [Kitchen] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [Kitchen] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [Kitchen] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [Kitchen] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [Kitchen] SET RECOVERY FULL 
GO
ALTER DATABASE [Kitchen] SET  MULTI_USER 
GO
ALTER DATABASE [Kitchen] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [Kitchen] SET DB_CHAINING OFF 
GO
ALTER DATABASE [Kitchen] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [Kitchen] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [Kitchen] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [Kitchen] SET ACCELERATED_DATABASE_RECOVERY = OFF  
GO
EXEC sys.sp_db_vardecimal_storage_format N'Kitchen', N'ON'
GO
ALTER DATABASE [Kitchen] SET QUERY_STORE = ON
GO
ALTER DATABASE [Kitchen] SET QUERY_STORE (OPERATION_MODE = READ_WRITE, CLEANUP_POLICY = (STALE_QUERY_THRESHOLD_DAYS = 30), DATA_FLUSH_INTERVAL_SECONDS = 900, INTERVAL_LENGTH_MINUTES = 60, MAX_STORAGE_SIZE_MB = 1000, QUERY_CAPTURE_MODE = AUTO, SIZE_BASED_CLEANUP_MODE = AUTO, MAX_PLANS_PER_QUERY = 200, WAIT_STATS_CAPTURE_MODE = ON)
GO
USE [Kitchen]
GO
/****** Object:  Table [dbo].[categories]    Script Date: 2024/2/26 20:43:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[categories](
	[categoriesID] [int] NOT NULL,
	[categoriesName] [nvarchar](50) NULL,
	[categoriesContant] [nvarchar](max) NULL,
 CONSTRAINT [PK_categories] PRIMARY KEY CLUSTERED 
(
	[categoriesID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[comment]    Script Date: 2024/2/26 20:43:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[comment](
	[userID] [int] NOT NULL,
	[cpID] [int] NOT NULL,
	[plID] [int] IDENTITY(1,1) NOT NULL,
	[plcomment] [nvarchar](50) NULL,
	[date] [datetime] NULL,
	[userImg] [nvarchar](300) NULL,
	[NickName] [nvarchar](50) NULL,
 CONSTRAINT [PK_comment] PRIMARY KEY CLUSTERED 
(
	[userID] ASC,
	[cpID] ASC,
	[plID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cp]    Script Date: 2024/2/26 20:43:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cp](
	[cpID] [int] IDENTITY(1,1) NOT NULL,
	[cpTitle] [nvarchar](50) NULL,
	[cpImg] [nvarchar](200) NULL,
	[cpjj] [nvarchar](200) NULL,
	[cpContant] [nvarchar](max) NULL,
	[cpSC] [int] NULL,
	[cpDZ] [int] NULL,
	[cpPL] [int] NULL,
	[UserID] [int] NULL,
	[NickName] [nvarchar](50) NULL,
	[Photo] [nvarchar](300) NULL,
 CONSTRAINT [PK_cp] PRIMARY KEY CLUSTERED 
(
	[cpID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[scdzCP]    Script Date: 2024/2/26 20:43:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[scdzCP](
	[userID] [int] NOT NULL,
	[cpID] [int] NOT NULL,
	[sc] [int] NULL,
	[dz] [int] NULL,
	[cpImg] [nvarchar](200) NULL,
	[cpTitle] [nvarchar](50) NULL,
 CONSTRAINT [PK_scdzCP_1] PRIMARY KEY CLUSTERED 
(
	[userID] ASC,
	[cpID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[user]    Script Date: 2024/2/26 20:43:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[user](
	[UserID] [int] IDENTITY(1,1) NOT NULL,
	[PassWord] [varchar](50) NULL,
	[Mobile] [varchar](11) NULL,
	[Photo] [nvarchar](300) NULL,
	[NickName] [nvarchar](50) NULL,
	[Salt] [nvarchar](10) NULL,
	[Grjs] [nvarchar](300) NULL,
	[Sex] [int] NULL,
	[Birthday] [datetime] NULL,
 CONSTRAINT [PK_user] PRIMARY KEY CLUSTERED 
(
	[UserID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[categories] ([categoriesID], [categoriesName], [categoriesContant]) VALUES (1, N'人气榜单', N'[{
		"ID": 1,
		"Title": "本周热门",
		"Cover": "https://i2.chuimg.com/62140e3fc4e24042aeb95b71fc100729_1178w_883h.jpg?imageView2/1/w/215/h/136/interlace/1/q/75"
	},
	{
		"ID": 2,
		"Title": "跟做榜",
		"Cover": "https://i2.chuimg.com/62140e3fc4e24042aeb95b71fc100729_1178w_883h.jpg?imageView2/1/w/215/h/136/interlace/1/q/75"
	},
	{
		"ID": 3,
		"Title": "收藏榜",
		"Cover": "https://i2.chuimg.com/62140e3fc4e24042aeb95b71fc100729_1178w_883h.jpg?imageView2/1/w/215/h/136/interlace/1/q/75"
	},
	{
		"ID": 4,
		"Title": "新秀菜谱",
		"Cover": "https://i2.chuimg.com/62140e3fc4e24042aeb95b71fc100729_1178w_883h.jpg?imageView2/1/w/215/h/136/interlace/1/q/75"
	},
	{
		"ID": 5,
		"Title": "潜力菜谱",
		"Cover": "https://i2.chuimg.com/62140e3fc4e24042aeb95b71fc100729_1178w_883h.jpg?imageView2/1/w/215/h/136/interlace/1/q/75"
	}
]')
INSERT [dbo].[categories] ([categoriesID], [categoriesName], [categoriesContant]) VALUES (2, N'时令食材', N'[{
		"ID": 1,
		"Title": "草莓",
		"Cover": "https://i2.chuimg.com/5b24b6c9a29243d08a0d0133974b249d_3120w_4160h.jpg?imageView2/1/w/300/h/140/interlace/1/q/75"
	},
	{
		"ID": 2,
		"Title": "羊排",
		"Cover": "https://i2.chuimg.com/5b24b6c9a29243d08a0d0133974b249d_3120w_4160h.jpg?imageView2/1/w/300/h/140/interlace/1/q/75"
	},
	{
		"ID": 3,
		"Title": "白菜",
		"Cover": "https://i2.chuimg.com/5b24b6c9a29243d08a0d0133974b249d_3120w_4160h.jpg?imageView2/1/w/300/h/140/interlace/1/q/75"
	},
	{
		"ID": 4,
		"Title": "萝卜",
		"Cover": "https://i2.chuimg.com/5b24b6c9a29243d08a0d0133974b249d_3120w_4160h.jpg?imageView2/1/w/300/h/140/interlace/1/q/75"
	},
	{
		"ID": 5,
		"Title": "油菜",
		"Cover": "https://i2.chuimg.com/5b24b6c9a29243d08a0d0133974b249d_3120w_4160h.jpg?imageView2/1/w/300/h/140/interlace/1/q/75"
	}
]')
INSERT [dbo].[categories] ([categoriesID], [categoriesName], [categoriesContant]) VALUES (3, N'热门分类', N'[{
	"ID": 1,
	"Title": "家常菜",
	"Cover": "https://i2.chuimg.com/b14a05675e6e438daa14ad8674bb0e75_1920w_2560h.jpg?imageView2/1/w/215/h/136/interlace/1/q/75"
}, {
	"ID": 2,
	"Title": "快手菜",
	"Cover": "https://i2.chuimg.com/b14a05675e6e438daa14ad8674bb0e75_1920w_2560h.jpg?imageView2/1/w/215/h/136/interlace/1/q/75"
}, {
	"ID": 3,
	"Title": "年夜饭",
	"Cover": "https://i2.chuimg.com/b14a05675e6e438daa14ad8674bb0e75_1920w_2560h.jpg?imageView2/1/w/215/h/136/interlace/1/q/75"
}, {
	"ID": 4,
	"Title": "下饭菜",
	"Cover": "https://i2.chuimg.com/b14a05675e6e438daa14ad8674bb0e75_1920w_2560h.jpg?imageView2/1/w/215/h/136/interlace/1/q/75"
}, {
	"ID": 5,
	"Title": "凉菜",
	"Cover": "https://i2.chuimg.com/b14a05675e6e438daa14ad8674bb0e75_1920w_2560h.jpg?imageView2/1/w/215/h/136/interlace/1/q/75"
}]')
INSERT [dbo].[categories] ([categoriesID], [categoriesName], [categoriesContant]) VALUES (4, N'菜系', N'[{
	"ID": 1,
	"Title": "粤菜",
	"Cover": "https://i2.chuimg.com/e21ef3dada4940fcbeecec12144589ec_808w_540h.jpg?imageView2/1/w/215/h/136/interlace/1/q/75"
}, {
	"ID": 2,
	"Title": "川菜",
	"Cover": "https://i2.chuimg.com/e21ef3dada4940fcbeecec12144589ec_808w_540h.jpg?imageView2/1/w/215/h/136/interlace/1/q/75"
}, {
	"ID": 3,
	"Title": "湘菜",
	"Cover": "https://i2.chuimg.com/e21ef3dada4940fcbeecec12144589ec_808w_540h.jpg?imageView2/1/w/215/h/136/interlace/1/q/75"
}, {
	"ID": 4,
	"Title": "鲁菜",
	"Cover": "https://i2.chuimg.com/e21ef3dada4940fcbeecec12144589ec_808w_540h.jpg?imageView2/1/w/215/h/136/interlace/1/q/75"
}, {
	"ID": 5,
	"Title": "东北菜",
	"Cover": "https://i2.chuimg.com/e21ef3dada4940fcbeecec12144589ec_808w_540h.jpg?imageView2/1/w/215/h/136/interlace/1/q/75"
}]')
GO
SET IDENTITY_INSERT [dbo].[comment] ON 

INSERT [dbo].[comment] ([userID], [cpID], [plID], [plcomment], [date], [userImg], [NickName]) VALUES (8, 1, 2, N'麻婆豆腐超下饭', CAST(N'2024-02-23T16:00:55.403' AS DateTime), N'https://localhost:44300/InvoiceStaticFile/2024/2/17/3Bg2KsWxx8Dn5e735f7e06f5345ca3d5bc47117cb3b1.jpg', N'阿包')
INSERT [dbo].[comment] ([userID], [cpID], [plID], [plcomment], [date], [userImg], [NickName]) VALUES (8, 4, 3, N'没吃过，难评', CAST(N'2024-02-23T16:26:27.303' AS DateTime), N'https://localhost:44300/InvoiceStaticFile/2024/2/17/3Bg2KsWxx8Dn5e735f7e06f5345ca3d5bc47117cb3b1.jpg', N'阿包')
INSERT [dbo].[comment] ([userID], [cpID], [plID], [plcomment], [date], [userImg], [NickName]) VALUES (8, 6, 4, N'流口水了，哎嘿', CAST(N'2024-02-23T16:27:54.647' AS DateTime), N'https://localhost:44300/InvoiceStaticFile/2024/2/17/3Bg2KsWxx8Dn5e735f7e06f5345ca3d5bc47117cb3b1.jpg', N'阿包')
INSERT [dbo].[comment] ([userID], [cpID], [plID], [plcomment], [date], [userImg], [NickName]) VALUES (8, 6, 5, N'流口水了，哎嘿', CAST(N'2024-02-23T16:31:50.863' AS DateTime), N'https://localhost:44300/InvoiceStaticFile/2024/2/17/3Bg2KsWxx8Dn5e735f7e06f5345ca3d5bc47117cb3b1.jpg', N'阿包')
INSERT [dbo].[comment] ([userID], [cpID], [plID], [plcomment], [date], [userImg], [NickName]) VALUES (8, 6, 6, N'嘿嘿', CAST(N'2024-02-23T16:32:30.853' AS DateTime), N'https://localhost:44300/InvoiceStaticFile/2024/2/17/3Bg2KsWxx8Dn5e735f7e06f5345ca3d5bc47117cb3b1.jpg', N'阿包')
INSERT [dbo].[comment] ([userID], [cpID], [plID], [plcomment], [date], [userImg], [NickName]) VALUES (9, 2, 1, N'番茄炒蛋世界无敌', CAST(N'2024-01-26T00:00:00.000' AS DateTime), N'https://tse1-mm.cn.bing.net/th/id/OIP-C.qTkt8yS_pbWDEpMkJcFcTAAAAA?w=197&h=197&c=7&r=0&o=5&dpr=2&pid=1.7', N'阿村')
SET IDENTITY_INSERT [dbo].[comment] OFF
GO
SET IDENTITY_INSERT [dbo].[cp] ON 

INSERT [dbo].[cp] ([cpID], [cpTitle], [cpImg], [cpjj], [cpContant], [cpSC], [cpDZ], [cpPL], [UserID], [NickName], [Photo]) VALUES (1, N'麻婆豆腐', N'https://ts1.cn.mm.bing.net/th?id=OSK.3bcfffa9b4fbdb3a1034db146606daa6&w=612&h=459&c=7&rs=1&qlt=80&o=6&cdv=1&dpr=2&pid=16.1', NULL, N'[{"ID":1,"Cover":"https://i2.chuimg.com/c39545a1f651437d8c9c72cf9facf38d_3200w_2558h.jpg?imageView2/2/w/300/interlace/1/q/75","Step":"豆腐洗净,切成2-3厘米的方块"},{"ID":2,"Cover":"https://i2.chuimg.com/948d31f4defb4e29803f1e5423003f46_3470w_2774h.jpg?imageView2/2/w/300/interlace/1/q/75","Step":"放入腌制好的肉馅,加入1勺豆瓣,放入豆腐"}]', 11, 11, 14, 8, N'阿珊', N'https://img.zcool.cn/community/0143395f110b9fa801215aa060a140.png@2o.png')
INSERT [dbo].[cp] ([cpID], [cpTitle], [cpImg], [cpjj], [cpContant], [cpSC], [cpDZ], [cpPL], [UserID], [NickName], [Photo]) VALUES (2, N'番茄炒蛋', N'https://i2.chuimg.com/19eb18e0f8e8484e8918536bebea3fe4_2754w_2202h.jpg?imageView2/2/w/660/interlace/1/q/75', N'我的番茄炒蛋是天下第一', N'[{"ID":1,"Cover":"https://i2.chuimg.com/6af98886193647d3bf0069a3f1fb9a13_3024w_3024h.jpg?imageView2/2/w/300/interlace/1/q/75","Step":"西红柿用开水烫下，去皮，切成滚刀块备用"},{"ID":2,"Cover":"https://i2.chuimg.com/4c32fa9a52e5488fa405aebe806007f5_3024w_3024h.jpg?imageView2/2/w/300/interlace/1/q/75","Step":"准备两个公鸡蛋，顺着一个方向打散"},{"ID":3,"Cover":"https://i2.chuimg.com/f79f6dbdf08e44a4b760e5d259fa2c9d_3024w_3024h.jpg?imageView2/2/w/300/interlace/1/q/75","Step":"加入炒好的鸡蛋还有番茄 ，翻炒均匀即可盛出"}]', 24, 24, 23, 9, N'阿村', N'https://tse1-mm.cn.bing.net/th/id/OIP-C.qTkt8yS_pbWDEpMkJcFcTAAAAA?w=197&h=197&c=7&r=0&o=5&dpr=2&pid=1.7')
INSERT [dbo].[cp] ([cpID], [cpTitle], [cpImg], [cpjj], [cpContant], [cpSC], [cpDZ], [cpPL], [UserID], [NickName], [Photo]) VALUES (4, N'玉子豆腐虾仁蒸蛋', N'https://i2.chuimg.com/575482d6fad84a659cb6035c70031d7f_3024w_4032h.jpg?imageView2/2/w/660/interlace/1/q/75', N'玉子豆腐️软嫩弹滑，吃起来像有韧性的鸡蛋羹', NULL, 0, 1, 2, 8, N'阿珊', N'https://img.zcool.cn/community/0143395f110b9fa801215aa060a140.png@2o.png')
INSERT [dbo].[cp] ([cpID], [cpTitle], [cpImg], [cpjj], [cpContant], [cpSC], [cpDZ], [cpPL], [UserID], [NickName], [Photo]) VALUES (5, N'番茄土豆肥牛汤', N'https://i2.chuimg.com/9e40635f20914169ae2dd5a957177953_1534w_2046h.jpg?imageView2/2/w/660/interlace/1/q/75', N'超超超开胃又好喝的番茄土豆肥牛汤', NULL, 2, 2, 2, 8, N'阿珊', N'https://img.zcool.cn/community/0143395f110b9fa801215aa060a140.png@2o.png')
INSERT [dbo].[cp] ([cpID], [cpTitle], [cpImg], [cpjj], [cpContant], [cpSC], [cpDZ], [cpPL], [UserID], [NickName], [Photo]) VALUES (6, N'椒麻口水鸡', N'https://i2.chuimg.com/c8180437f154491caa2915a5109304e5_2987w_3983h.jpg?imageView2/2/w/660/interlace/1/q/75', N'麻辣鲜香，超级入味', NULL, 3, 3, 6, 8, N'阿珊', N'https://img.zcool.cn/community/0143395f110b9fa801215aa060a140.png@2o.png')
INSERT [dbo].[cp] ([cpID], [cpTitle], [cpImg], [cpjj], [cpContant], [cpSC], [cpDZ], [cpPL], [UserID], [NickName], [Photo]) VALUES (7, N'蒜香黑椒牛肉粒', N'https://i2.chuimg.com/6087c41271ec45d5b9534d5dd14fcdbe_960w_1280h.jpg?imageView2/2/w/660/interlace/1/q/75', N'牛肉粒鲜嫩爆汁，蒜香十足', NULL, 4, 2, 4, 8, N'阿珊', N'https://img.zcool.cn/community/0143395f110b9fa801215aa060a140.png@2o.png')
INSERT [dbo].[cp] ([cpID], [cpTitle], [cpImg], [cpjj], [cpContant], [cpSC], [cpDZ], [cpPL], [UserID], [NickName], [Photo]) VALUES (8, N'耗油生菜', N'https://i2.chuimg.com/2709da07977142bc9c71045d72adecdf_1080w_1438h.jpg?imageView2/2/w/660/interlace/1/q/75', N'减肥餐必备菜谱', NULL, 5, 4, 5, 8, N'阿珊', N'https://img.zcool.cn/community/0143395f110b9fa801215aa060a140.png@2o.png')
INSERT [dbo].[cp] ([cpID], [cpTitle], [cpImg], [cpjj], [cpContant], [cpSC], [cpDZ], [cpPL], [UserID], [NickName], [Photo]) VALUES (9, N'糖醋排骨', N'https://i2.chuimg.com/7d3478b91f9a44c1bffe90a946fb069a_1242w_1242h.jpg?imageView2/2/w/660/interlace/1/q/75', N'孜然牛肉糖醋排骨', NULL, 6, 5, 6, 8, N'阿珊', N'https://img.zcool.cn/community/0143395f110b9fa801215aa060a140.png@2o.png')
INSERT [dbo].[cp] ([cpID], [cpTitle], [cpImg], [cpjj], [cpContant], [cpSC], [cpDZ], [cpPL], [UserID], [NickName], [Photo]) VALUES (10, N'香菜拌牛肉', N'https://i2.chuimg.com/ebcb90ae1b7742aca9c490b235b370f2_713w_951h.jpg?imageView2/2/w/660/interlace/1/q/75', N'每天少不了一盘凉拌菜', NULL, 24, 7, 7, 8, N'阿珊', N'https://img.zcool.cn/community/0143395f110b9fa801215aa060a140.png@2o.png')
INSERT [dbo].[cp] ([cpID], [cpTitle], [cpImg], [cpjj], [cpContant], [cpSC], [cpDZ], [cpPL], [UserID], [NickName], [Photo]) VALUES (11, N'青椒肉丝', N'https://i2.chuimg.com/69c86589720a4d0f8d81a584c552404e_1080w_1303h.jpg?imageView2/2/w/660/interlace/1/q/75', N'五星级饭店的厨师那里偷学的做法', NULL, 8, 7, 15, 8, N'阿珊', N'https://img.zcool.cn/community/0143395f110b9fa801215aa060a140.png@2o.png')
INSERT [dbo].[cp] ([cpID], [cpTitle], [cpImg], [cpjj], [cpContant], [cpSC], [cpDZ], [cpPL], [UserID], [NickName], [Photo]) VALUES (12, N'鱼香肉丝', N'https://i2.chuimg.com/503615c4419940f7b212a4c03f214caa_1280w_733h.jpg?imageView2/2/w/660/interlace/1/q/75', N'美食需要分享', NULL, 9, 9, 9, 8, N'阿珊', N'https://img.zcool.cn/community/0143395f110b9fa801215aa060a140.png@2o.png')
SET IDENTITY_INSERT [dbo].[cp] OFF
GO
INSERT [dbo].[scdzCP] ([userID], [cpID], [sc], [dz], [cpImg], [cpTitle]) VALUES (8, 1, 0, 1, N'https://ts1.cn.mm.bing.net/th?id=OSK.3bcfffa9b4fbdb3a1034db146606daa6&w=612&h=459&c=7&rs=1&qlt=80&o=6&cdv=1&dpr=2&pid=16.1', N'麻婆豆腐')
INSERT [dbo].[scdzCP] ([userID], [cpID], [sc], [dz], [cpImg], [cpTitle]) VALUES (8, 2, 1, 1, N'https://i2.chuimg.com/19eb18e0f8e8484e8918536bebea3fe4_2754w_2202h.jpg?imageView2/2/w/660/interlace/1/q/75', N'番茄炒蛋')
INSERT [dbo].[scdzCP] ([userID], [cpID], [sc], [dz], [cpImg], [cpTitle]) VALUES (8, 4, 0, 0, N'https://i2.chuimg.com/575482d6fad84a659cb6035c70031d7f_3024w_4032h.jpg?imageView2/2/w/660/interlace/1/q/75', N'玉子豆腐虾仁蒸蛋')
INSERT [dbo].[scdzCP] ([userID], [cpID], [sc], [dz], [cpImg], [cpTitle]) VALUES (8, 6, 0, 0, N'https://i2.chuimg.com/c8180437f154491caa2915a5109304e5_2987w_3983h.jpg?imageView2/2/w/660/interlace/1/q/75', N'椒麻口水鸡')
INSERT [dbo].[scdzCP] ([userID], [cpID], [sc], [dz], [cpImg], [cpTitle]) VALUES (8, 7, 0, 0, N'https://i2.chuimg.com/6087c41271ec45d5b9534d5dd14fcdbe_960w_1280h.jpg?imageView2/2/w/660/interlace/1/q/75', N'蒜香黑椒牛肉粒')
INSERT [dbo].[scdzCP] ([userID], [cpID], [sc], [dz], [cpImg], [cpTitle]) VALUES (8, 8, 0, 0, N'https://i2.chuimg.com/2709da07977142bc9c71045d72adecdf_1080w_1438h.jpg?imageView2/2/w/660/interlace/1/q/75', N'耗油生菜')
INSERT [dbo].[scdzCP] ([userID], [cpID], [sc], [dz], [cpImg], [cpTitle]) VALUES (8, 9, 0, 0, N'https://i2.chuimg.com/7d3478b91f9a44c1bffe90a946fb069a_1242w_1242h.jpg?imageView2/2/w/660/interlace/1/q/75', N'糖醋排骨')
INSERT [dbo].[scdzCP] ([userID], [cpID], [sc], [dz], [cpImg], [cpTitle]) VALUES (8, 10, 0, 0, N'https://i2.chuimg.com/ebcb90ae1b7742aca9c490b235b370f2_713w_951h.jpg?imageView2/2/w/660/interlace/1/q/75', N'香菜拌牛肉')
INSERT [dbo].[scdzCP] ([userID], [cpID], [sc], [dz], [cpImg], [cpTitle]) VALUES (8, 11, 0, 0, N'https://i2.chuimg.com/69c86589720a4d0f8d81a584c552404e_1080w_1303h.jpg?imageView2/2/w/660/interlace/1/q/75', N'青椒肉丝')
INSERT [dbo].[scdzCP] ([userID], [cpID], [sc], [dz], [cpImg], [cpTitle]) VALUES (8, 12, 0, 0, N'https://i2.chuimg.com/503615c4419940f7b212a4c03f214caa_1280w_733h.jpg?imageView2/2/w/660/interlace/1/q/75', N'鱼香肉丝')
GO
SET IDENTITY_INSERT [dbo].[user] ON 

INSERT [dbo].[user] ([UserID], [PassWord], [Mobile], [Photo], [NickName], [Salt], [Grjs], [Sex], [Birthday]) VALUES (8, N'71A31D783CC08A4C2C4FC3B5462A9783', N'15018101437', N'https://localhost:44300/InvoiceStaticFile/2024/2/17/3Bg2KsWxx8Dn5e735f7e06f5345ca3d5bc47117cb3b1.jpg', N'阿包', N'4698', N'一个爱吃蛋糕的小女孩', 1, CAST(N'2002-02-17T00:00:00.000' AS DateTime))
INSERT [dbo].[user] ([UserID], [PassWord], [Mobile], [Photo], [NickName], [Salt], [Grjs], [Sex], [Birthday]) VALUES (9, N'DA88237E5A4BFF70919F9316D12CBE6', N'13715735253', N'https://tse1-mm.cn.bing.net/th/id/OIP-C.qTkt8yS_pbWDEpMkJcFcTAAAAA?w=197&h=197&c=7&r=0&o=5&dpr=2&pid=1.7', N'阿村', N'2705', N'爱炒粉的boy', 0, CAST(N'2024-01-25T16:22:12.710' AS DateTime))
INSERT [dbo].[user] ([UserID], [PassWord], [Mobile], [Photo], [NickName], [Salt], [Grjs], [Sex], [Birthday]) VALUES (10, N'3393527DC07FCD8432C5DE1ECCD36E0', N'13652815770', N'', N'', N'1916', N'', 0, CAST(N'2024-02-15T23:41:45.297' AS DateTime))
SET IDENTITY_INSERT [dbo].[user] OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'1收藏0没有' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'scdzCP', @level2type=N'COLUMN',@level2name=N'sc'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'1点赞0没有' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'scdzCP', @level2type=N'COLUMN',@level2name=N'dz'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'手机号' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'user', @level2type=N'COLUMN',@level2name=N'Mobile'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'头像' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'user', @level2type=N'COLUMN',@level2name=N'Photo'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'昵称' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'user', @level2type=N'COLUMN',@level2name=N'NickName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'个人介绍' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'user', @level2type=N'COLUMN',@level2name=N'Grjs'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'性别(男01女)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'user', @level2type=N'COLUMN',@level2name=N'Sex'
GO
USE [master]
GO
ALTER DATABASE [Kitchen] SET  READ_WRITE 
GO
