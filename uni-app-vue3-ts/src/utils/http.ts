import { useMemberStore } from '../stores/modules/member'

const baseURL = 'https://localhost:44300/api'

// 添加拦截器
const httpInterceptor = {
  // 拦截前触发
  invoke(options: UniApp.RequestOptions) {
    // 非http开头需拼接地址
    if (!options.url.startsWith('http')) {
      options.url = baseURL + options.url
    }
    // 请求超时，默认60s
    options.timeout = 10000
    options.header = {
      ...options.header,
    }
    // 添加token请求头标识
    const meberStore = useMemberStore()
    const token = meberStore.profile?.Token
    if (token) {
      // Bearer后面记得带空格
      options.header.Authorization = 'Bearer ' + token
    }
    console.log(options)
  },
}
uni.addInterceptor('request', httpInterceptor)
uni.addInterceptor('uploadFile', httpInterceptor)

// 给后端返回的数据指定类型
interface Data<T> {
  Code: string
  Message: string
  Result: T
}

//请求函数
export const http = <T>(options: UniApp.RequestOptions) => {
  //返回promise对象
  return new Promise<Data<T>>((resolve, reject) => {
    uni.request({
      ...options,
      //请求成功
      success(res) {
        // 状态码 2xx,axios设计原理
        if (res.statusCode >= 200 && res.statusCode < 300) {
          // as ... 类型断言
          resolve(res.data as Data<T>)
        } else if (res.statusCode === 401) {
          // 401错误 token过期
          // 清理用户信息，跳转到登录页
          const meberStore = useMemberStore()
          meberStore.clearProfile()
          uni.navigateTo({ url: '/pages/login/login' })
          // 标记失败
          reject(res)
        } else {
          // 其他错误  根据后端错误信息轻提示
          uni.showToast({
            icon: 'none',
            title: (res.data as Data<T>).Message || '请求错误',
          })
          // 标记失败
          reject(res)
        }
      },
      //响应失败
      fail(err) {
        // 网络错误
        uni.showToast({
          icon: 'none',
          title: '网络错误,换个网络试试',
        })
        // 标记失败
        reject(err)
      },
    })
  })
}
