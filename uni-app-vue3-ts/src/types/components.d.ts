// Vloar 提供全局注册组件
import ZcfCard from '@/components/ZcfCard.vue'
declare module 'vue' {
  export interface GlobalComponents {
    ZcfCard: typeof ZcfCard
  }
}

// 组件实例类型
export type CardInstance = InstanceType<typeof ZcfCard>
