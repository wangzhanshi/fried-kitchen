/** 用户信息 */
export type BaseProfile = {
  /** 登录凭证 */
  Token: string
  /** 用户头像  */
  Photo: string
  /** 昵称 */
  NickName: string
  /** 个人介绍 */
  Grjs: string
  /** 点赞菜谱 */
  LikeList: LikeCpParams[]
  /** 发表菜谱 */
  PublishList: BaseCpParams[]
}

/** 登录用户信息 */
export type LoginParams = {
  /** 手机号 */
  mobile: string
  /** 密码 */
  passWord: string
  /** 密码二次确认 不用传向后端*/
  newPassword?: string
}

/** 点赞菜谱信息 */
export type LikeCpParams = BaseCpParams & {
  /** 用户ID */
  UserId: number
  /**收藏*/
  Sc: number
  /**点赞*/
  Dz: number
}
/** 通用菜谱信息 */
export type BaseCpParams = {
  /**菜谱ID*/
  CpId: number
  /**菜谱标题*/
  CpTitle: string
  /**菜谱照片*/
  CpImg: string
}
