import type { CpItem } from './index'
/**菜谱详情 */
export type CpResult = {
  CpList: CpItem[]
  CommentList: CommentItem[]
}
/**评论列表 */
export type CommentItem = Pick<CpItem, 'CpId' | 'UserId' | 'NickName'> & {
  /**评论ID*/
  PlId: number
  /**评论内容 */
  Plcomment: string
  /**评论时间*/
  Date: string
  /**作者ID*/
  UserId: number
  /**作者头像*/
  UserImg: string
}
/** 用户操作菜谱信息 */
export type SdpCpParams = {
  /** 评论 */
  Pl: number
  /**收藏*/
  Sc: number
  /**点赞*/
  Dz: number
}
/** 用户评论菜谱信息 */
export type CommentParams = Pick<CommentItem, 'CpId' | 'Plcomment'>
