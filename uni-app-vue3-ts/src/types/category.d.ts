/**一级分类项 */
export type CategoryItem = {
  /**一级分类ID */
  CategoriesId: number
  /**一级分类名称 */
  CategoriesName: string
  /**二级分类集合 */
  CategoriesContant: ContantItem[]
}
/**二级分类项 */
export type ContantItem = {
  /**二级分类ID */
  ID: number
  /**二级分类标题 */
  Title: string
  /**二级分类照片 */
  Cover: string
}
