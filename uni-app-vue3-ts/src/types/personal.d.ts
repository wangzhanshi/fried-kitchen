/** 用户信息 */
export type profileParams = {
  /** 用户头像  */
  Photo?: string
  /** 昵称 */
  NickName: string
  /** 个人介绍 */
  Grjs: string
  /** 性别 */
  Sex?: number
  /** 生日 */
  Birthday?: string
}
