/**首页 菜谱列表数据类型*/
export type CpItem = {
  /**菜谱ID*/
  CpId: number
  /**菜谱标题*/
  CpTitle: string
  /**菜谱照片*/
  CpImg: string
  /**菜谱简介*/
  Cpjj: string
  /**菜谱步骤*/
  CpContant: ContantParams[]
  /**收藏*/
  CpSc: number
  /**点赞*/
  CpDz: number
  /**评论*/
  CpPl: number
  /**作者ID*/
  UserId: number
  /**作者名*/
  NickName: string
  /**作者头像*/
  Photo: string
}
/**首页 通用分页结果类型*/
export type PageResult<T> = {
  /**菜谱列表数据*/
  CpList: T[]
  /**总条数*/
  Counts: number
  /**当前页数*/
  Page: number
  /**总页数*/
  Pages: number
  /**每页条数*/
  PageSize: number
}
/**首页 通用分页参数类型*/
export type PageParams = {
  /**当前页数*/
  Page?: number
  /**每页条数*/
  PageSize?: number
}
/**首页 菜谱步骤信息*/
export type ContantParams = {
  /**步骤ID*/
  ID: number
  /**步骤照片*/
  Cover: string
  /**步骤描述*/
  Step: string
}
