import type { BaseProfile, LoginParams } from '@/types/member'
import { http } from '@/utils/http'
/**
 * 用户登录
 * @param data 登录参数
 */
export const getLoginAPI = (data: LoginParams) => {
  return http<BaseProfile>({
    method: 'POST',
    url: '/User/Login',
    data,
  })
}
/**
 * 用户注册
 * @param data 注册参数
 */
export const getRegisterAPI = (data: LoginParams) => {
  return http({
    method: 'POST',
    url: '/User/Register',
    data,
  })
}
