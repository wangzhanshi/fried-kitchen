import { http } from '@/utils/http'
import type { LikeCpParams } from '@/types/member'
/**
 * 获取收藏菜谱
 */
export const getCollectionAPI = () => {
  return http<LikeCpParams[]>({
    method: 'GET',
    url: '/CP/GetScCp',
  })
}
/**
 * 搜索收藏菜谱
 */
export const getSelectScAPI = (keyword: string) => {
  return http<LikeCpParams[]>({
    method: 'GET',
    url: '/CP/GetSelectScCp',
    data: {
      keyword,
    },
  })
}
