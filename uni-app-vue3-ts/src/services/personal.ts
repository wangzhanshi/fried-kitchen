import { http } from '@/utils/http'
import type { profileParams } from '@/types/personal'
/**
 * 获取个人信息
 */
export const getUserAPI = () => {
  return http<profileParams>({
    method: 'GET',
    url: '/User/GetUserInfo',
  })
}
/**
 * 修改个人信息
 * @param data 修改信息参数
 */
export const UpdateUserAPI = (data: profileParams) => {
  return http<profileParams>({
    method: 'POST',
    url: '/User/UpdateUserInfo',
    data,
  })
}
