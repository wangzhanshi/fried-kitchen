import type { CpItem, PageParams, PageResult } from '@/types/index'
import { http } from '@/utils/http'
/**
 * 菜谱列表
 * @param data 分页参数
 */
export const getIndexCpAPI = (data?: PageParams) => {
  return http<PageResult<CpItem>>({
    method: 'GET',
    url: '/CP/GetCp',
    data,
  })
}
