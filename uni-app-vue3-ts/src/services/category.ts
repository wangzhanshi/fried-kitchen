import type { CategoryItem } from '@/types/category'
import { http } from '@/utils/http'
/**
 * 分类列表
 */
export const getCategoryAPI = () => {
  return http<CategoryItem[]>({
    method: 'GET',
    url: '/CP/GetCpCategory',
  })
}
