import type { CpItem } from '@/types/index'
import { http } from '@/utils/http'
/**
 * 搜索菜谱
 * @param keyword 关键字
 */
export const getSelectCpAPI = (keyword: string) => {
  return http<CpItem[]>({
    method: 'GET',
    url: '/CP/GetSelectCp',
    data: {
      keyword,
    },
  })
}
