import type { CommentItem, CommentParams, CpResult, SdpCpParams } from '@/types/caipu'
import type { LikeCpParams } from '@/types/member'
import { http } from '@/utils/http'
/**
 * 菜谱详情
 * @param CpId 菜谱ID
 */
export const getCpByIdAPI = (CpId: number) => {
  return http<CpResult>({
    method: 'GET',
    url: '/CP/GetCpInfo',
    data: {
      CpId,
    },
  })
}
/**
 * 获取用户对菜谱的操作
 * @param CpId 菜谱ID
 */
export const getSdpCpdAPI = (CpId: number) => {
  return http<SdpCpParams>({
    method: 'GET',
    url: '/CP/GetSDPCp',
    data: {
      CpId,
    },
  })
}
/**
 * 用户点赞or取消
 * @param data 点赞参数
 */
export const ChangeDzdAPI = (data: { CpId: number; Dz: number }) => {
  return http<LikeCpParams[]>({
    method: 'POST',
    url: '/CP/LikeCP',
    data,
  })
}
/**
 * 用户收藏or取消
 * @param data 收藏参数
 */
export const ChangeScAPI = (data: { CpId: number; Sc: number }) => {
  return http({
    method: 'POST',
    url: '/CP/CollectionCP',
    data,
  })
}
/**
 * 用户评论
 * @param data 评论参数
 */
export const AddCommentAPI = (data: CommentParams) => {
  return http<CommentItem[]>({
    method: 'POST',
    url: '/CP/CommentCP',
    data,
  })
}
